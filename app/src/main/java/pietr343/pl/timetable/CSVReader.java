package pietr343.pl.timetable;

import java.io.BufferedReader;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pietr343 on 11.06.16.
 */
public class CSVReader {
    InputStream inputStream;


    public CSVReader(InputStream is){
        this.inputStream=is;
    }

    public List<Lesson> read(String week,String day ){
        List<Lesson> resultList = new ArrayList<Lesson>();
        List<String[]> tempList = new ArrayList<String []>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        try{
            String csvLine;
            while((csvLine= reader.readLine())!=null){
                String[] row = csvLine.split(",");
                Lesson tmp = new Lesson(row[5],row[3] + "  (" + row[2] +")",row[4],row[6],row[7]);
                if(row[0].equals(week) && row[1].equals(day))
                resultList.add(tmp);
                //tempList.add(row);
            }


        }catch(IOException ex){
            throw new RuntimeException("Error in reading csv file"+ ex);
        }finally{
            try{
            inputStream.close();

            }catch(IOException e){
                throw new RuntimeException("Error while closing InputStream" + e);
            }
        }
        return resultList;
    }

}
