package pietr343.pl.timetable;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TabHost;

import java.util.ArrayList;
import java.util.List;

import pietr343.pl.timetable.fragments.FragmentCzwB;
import pietr343.pl.timetable.fragments.FragmentPnB;
import pietr343.pl.timetable.fragments.FragmentPtB;
import pietr343.pl.timetable.fragments.FragmentSrB;
import pietr343.pl.timetable.fragments.FragmentWtB;

public class WeekBActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener,TabHost.OnTabChangeListener {

    ViewPager viewPager;
    TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week_b);

        //android.support.v7.widget.Toolbar tb = (android.support.v7.widget.Toolbar)findViewById(R.id.toolBar);
        //setSupportActionBar(tb);

        //getActionBar().setDisplayHomeAsUpEnabled(true);

        initViewPager();
        initTabHost();
    }

    private void initTabHost() {
        tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        String[] tabNames = {"PON","WT","ŚR","CZW","PT"};

        for(int i=0; i<tabNames.length;i++){
            TabHost.TabSpec tabSpec;
            tabSpec = tabHost.newTabSpec(tabNames[i]);
            tabSpec.setIndicator(tabNames[i]);
            tabSpec.setContent(new FakeContent(getApplicationContext()));
            tabHost.addTab(tabSpec);
        }
        tabHost.setOnTabChangedListener(this);
    }

    public void onWeekBClick(MenuItem item) {

    }

    public void onWeekAClick(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }

    public void onAboutClick(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(),AboutActivity.class);
        startActivity(intent);
    }

    public class FakeContent implements TabHost.TabContentFactory{

        Context context;
        public FakeContent(Context mcontext){
            context=mcontext;
        }

        @Override
        public View createTabContent(String tag) {
            View fakeView = new View(context);
            fakeView.setMinimumHeight(0);
            fakeView.setMinimumWidth(0);
            return fakeView;
        }
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        List<android.support.v4.app.Fragment> listFragments = new ArrayList<android.support.v4.app.Fragment>();
        listFragments.add(new FragmentPnB());
        listFragments.add(new FragmentWtB());
        listFragments.add(new FragmentSrB());
        listFragments.add(new FragmentCzwB());
        listFragments.add(new FragmentPtB());

        MyFragmentAdapter myFragmentAdapter = new MyFragmentAdapter(getSupportFragmentManager(),listFragments);
        viewPager.setAdapter(myFragmentAdapter);
        viewPager.setOnPageChangeListener(this);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int selectedItem) {
        tabHost.setCurrentTab(selectedItem);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onTabChanged(String tabId) {
        int selectedItem = tabHost.getCurrentTab();
        viewPager.setCurrentItem(selectedItem);
        //TabHost ustawianie na środku
        HorizontalScrollView hScrollView = (HorizontalScrollView) findViewById(R.id.h_scroll_view);
        View tabView = tabHost.getCurrentTabView();
        int scrollPos = tabView.getLeft() -(hScrollView.getWidth() -tabView.getWidth())/2;
        hScrollView.smoothScrollTo(scrollPos,0);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


}