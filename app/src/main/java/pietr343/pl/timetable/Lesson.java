package pietr343.pl.timetable;

/**
 * Created by pietr343 on 11.06.16.
 */
public class Lesson {
    private String startTime;
    private String endTime;
    private String room;
    private String name;
    private String teacher;

    public Lesson(String room, String name, String teacher,String startTime,String endTime) {
        //super();
        this.room = room;
        this.name = name;
        this.teacher = teacher;
        this.endTime = endTime;
        this.startTime = startTime;
    }


    public String getName() {
        return name;
    }

    public String getRoom() {
        return room;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
}
