package pietr343.pl.timetable.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.List;

import pietr343.pl.timetable.CSVReader;
import pietr343.pl.timetable.CustomAdapter;
import pietr343.pl.timetable.Lesson;
import pietr343.pl.timetable.R;

/**
 * Created by pietr343 on 12.06.16.
 */
public class FragmentWt extends android.support.v4.app.Fragment {

    ListView listview;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v =inflater.inflate(R.layout.fragment_wt_layout,container,false);

        listview = (ListView) v.findViewById(R.id.listViewWT);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View v, int arg2,
                                    long arg3) {
                Toast.makeText(getContext(), "Kliknięto!!!", Toast.LENGTH_SHORT).show();
            }
        });


        CustomAdapter adapter = new CustomAdapter(this.getContext(),R.layout.lesson_view);

        Parcelable state = listview.onSaveInstanceState();
        listview.setAdapter(adapter);
        listview.onRestoreInstanceState(state);

        InputStream inputStream = getResources().openRawResource(R.raw.table);
        //InputStream inputStream = getResources().openRawResource(R.raw.F3A_L2);
        CSVReader csv = new CSVReader(inputStream);
        List<Lesson> lessonsList = csv.read("A","2");

        for(Lesson lessonsData : lessonsList){
            adapter.add(lessonsData);
        }

        return v;
    }
}
