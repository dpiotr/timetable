package pietr343.pl.timetable;

import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by pietr343 on 12.06.16.
 */
public class MyFragmentAdapter extends FragmentPagerAdapter {

    List<android.support.v4.app.Fragment> listFragments;

    public MyFragmentAdapter(FragmentManager fm, List<android.support.v4.app.Fragment> listFragments) {
        super(fm);
        this.listFragments = listFragments;
    }

    @Override
    public android.support.v4.app.Fragment getItem(int position) {
        return listFragments.get(position);
    }

    @Override
    public int getCount() {
        return listFragments.size();
    }
}
