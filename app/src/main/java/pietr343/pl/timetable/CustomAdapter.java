package pietr343.pl.timetable;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pietr343 on 11.06.16.
 */
public class CustomAdapter  extends ArrayAdapter {

    private List list = new ArrayList();

    public CustomAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public void add(Lesson object){
        list.add(object);
        super.add(object);
    }

    static class  Holder{
        TextView ROOM;
        TextView NAME;
        TextView TEACHER;
        TextView START_TIME;
        TextView END_TIME;
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        Holder holder;
        if(convertView==null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.lesson_view, parent, false);
            holder = new Holder();
            holder.ROOM = (TextView) row.findViewById(R.id.room);
            holder.NAME = (TextView) row.findViewById(R.id.name);
            holder.TEACHER = (TextView) row.findViewById(R.id.teacher);
            holder.START_TIME = (TextView) row.findViewById(R.id.start_time);
            holder.END_TIME = (TextView) row.findViewById(R.id.end_time);
            row.setTag(holder);
        }else{
            holder = (Holder) row.getTag();
        }
        Lesson LS = (Lesson) getItem(position);
        holder.ROOM.setText(LS.getRoom());
        holder.NAME.setText(LS.getName());
        holder.TEACHER.setText(LS.getTeacher());
        holder.START_TIME.setText(LS.getStartTime());
        holder.END_TIME.setText(LS.getEndTime());

        return row;
    }


}
